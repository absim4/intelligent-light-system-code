//Instead of forcing this into a 100 loop, I have instead instantiated a counter and an if statement. This will probably allow onCameraFrame to be called 
//as it regularly does.


//	put this in the class somewhere
//	output=mymethod();
// 	if (IDaccepted==1)
	//{
	//	"translate output (which is a number) into a thing."
	//}
	//

    public int j=1;
    public int i=2;
	public int IDaccepted=0;
    public int output; //this is the final output and is what the method will be returning. It will return the ID of the light which you are
    //situated under.
    public int h=1;
    public int k=1;


	public	byte[] light_ID0 = {0, 0, 1, 1};
	public	byte[] light_ID1 = {1, 0, 0, 0};
	public	byte[] light_ID2 = {1 ,1 ,1 ,1};
	public	byte[] light_ID3 = {1 ,0 ,1 ,0};
	public	byte thresh=125; //manually changed variable
    public	int[] check=new int [i];
    //i+1 is used because java uses zero indexing; MATLAB does not
    public	boolean ID_TL[]=new boolean  [101];
    public	boolean ID_TR[]=new boolean [101];
    public	boolean  ID_BL[]=new boolean [101];
    public	boolean  ID_BR[]=new boolean [101];

    //the above are booleans because int cannot easily transfer to boolean
    public	int check_o[]=new int [i+1];
    public	int check_z[]=new int [i+1];
    public 	double P_BL[]=new double [i+1];
    public	double P_TL[]=new double [i+1];
    public	double P_TR[]=new double [i+1];
    public	double P_BR[]=new double [i+1];
    public int mymethod1(){
            mymethod2(mat1);
            //call camera capture method here
            j=j+1;
            i=i+1;
        if (j>=100){
		check_z=equcompare(check,false);
        check_o=equcompare(check,true);
        double zros = 0;
        for(int re=0; re<check_z.length; re++ ) {
            zros+=check_z[re];
        }

        double ons = 0;
        for(int re=0; re<check_o.length; re++ ) {
            zros+=check_o[re];
        }

        double ratio=zros/ons;

        if ((ratio == 1) && (k < 10)){
            //fprintf("You are under light ID '0'\n");
            output=0;
        }

        if (ratio == 2)
        {    //fprintf("You are under light ID '1'\n");
            output=1;
        }

        if (ratio < 1)
        {  // fprintf("You are under light ID '2'\n");
            output=2;
        }

        if ((ratio == 1) && (k > 12))
        {
            output=3;
        }

        j=1;
        k=1;
        h=1;
        i=2;
		IDaccepted=1;
		}
		else IDaccepted=0;
		
		return output;
		

        
    }



    public void mymethod2(Mat mat1)
    {


        int dim[]={mat1.height(), mat1.width()};
        //	int dim[]= new int[]{5,5};

        //above commented statements were dummies to make sure Java compiled correctly with no access to the opencv library


        //notes for the below statements are as follows; Ryan wrote his code on a quadrant by quadrant basis, while I've written the code on a process
        //by process basis.
		double TL[][]=new double[dim[0]/2][dim[1]/2];
		double TR[][]=new double[dim[0]/2][dim[1]/2];
		double BR[][]=new double [dim[0]/2][dim[1]/2];
		double BL[][]=new double [ dim[0]/2][dim[1]/2];


			TL = subsection(mat1,  0, dim[0]/2, 0, dim[1]/2);
			BL = subsection(mat1,  dim[0]/2, dim[0], 0, dim[1]/2);
			TR = subsection(mat1,  0, dim[0]/2, dim[0]/2, dim[0]);
			BR = subsection(mat1,   dim[0]/2, dim[0], dim[0]/2, dim[0]);

        // takes a quartered subsection of each area



        TL = avgcompare(TL, thresh);
        BL = avgcompare(BL, thresh);
        BR = avgcompare(BR, thresh);
        TR = avgcompare(TR, thresh)	;

        // black-and-white comparison function
        P_TL[i] = sumarray(TL)/((TL[0].length*TL.length)*100);
        P_TR[i] = sumarray(TR)/((TR[0].length*TR.length)*100);
        P_BL[i]= sumarray(BL)/((BL[0].length*BL.length)*100);
        P_BR[i] = sumarray(BR)/((BR[0].length*BR.length)*100);
        //percentage brightness in each quadrant


        //threshold equations

        double threshold_TL=sumarray1D(P_TL)/(P_TL.length);
        double threshold_BL=sumarray1D(P_BL)/(P_BL.length);
        double threshold_TR=sumarray1D(P_TR)/(P_TR.length);
        doublethreshold_BR=sumarray1D(P_BR)/(P_BR.length);

        //different methods for summing 1D arrays and 2D arrays were necessary

        ID_TL[i]=P_TL[i]>threshold_TL;
        ID_BL[i]=P_BL[i]>threshold_BL;
        ID_BR[i]=P_BR[i]>threshold_BR;
        ID_TR[i]=P_TR[i]>threshold_TR;

        //

        if ((P_BL[i] >= P_TL[i]) && (P_BL[i] >= P_TR[i]) && (P_BL[i] >= P_BR[i])){
            if (ID_BL[i]=false) {
                check[i]=0;
            }
            else check[i]=1;
        }
            //check = ID_BL[i];


        if ((P_BR[i] >= P_TL[i]) && (P_BR[i] >= P_TR[i]) && (P_BR[i] >= P_BL[i]))
            if (ID_BR[i]=false) {
                check[i]=0;
            }
            else check[i]=1;

        if ((P_TL[i] >= P_BL[i]) && (P_TL[i] >= P_TR[i]) && (P_TL[i] >= P_BR[i]))
            if (ID_TL[i]=false) {
                check[i]=0;
            }
            else check[i]=1;

        if ((P_TR[i] >= P_TL[i]) && (P_TR[i] >= P_BL[i]) && (P_TR[i] >= P_BR[i]))
            if (ID_TR[i]=false) {
                check[i]=0;
            }
            else check[i]=1;

        if (check[i] == 1 && check[i-1]==0)
            k=k+1; //measuring rising edges

        if (check[i] ==0 && check[i-1]==1)
            h=h+1; //may not be necessary
    }




		public double[][] subsection(Mat mat1, int startingrow, int endingrow, int startingcolumn, int endingcolumn)
		{
			double[][][] newArray = new double[endingrow-startingrow][endingcolumn-startingcolumn][1]; // define new array 
			double[][] newerArray = new double[endingrow-startingrow][endingcolumn-startingcolumn];
			if ((endingrow!=startingrow) | (startingrow!=startingcolumn)){ //if it's a zero matrix
				
				for (int i =startingrow; i < endingrow;i++){ //for every row
				for (int j=startingcolumn; j< endingcolumn; j++)
						newArray[i][j]=mat.get(i,j);
					//	newerArray[i][j]=mat.get(i,j)[0];
						newerArray[i][j]=newArray[i][j][0];
					//http://answers.opencv.org/question/5/how-to-get-and-modify-the-pixel-of-mat-in-java/
					
				}
			}
			return newArray;
		}
//compiles fine


    public byte[][] avgcompare(byte[][]oldArray, int threshold)
    {
        double [][] newArray = new double [oldArray.length][oldArray[0].length]; //defining new array
        for (int i=0; i < oldArray.length; i++) //
        {
            for (int j=0; j< oldArray[0].length; j++)
            {
                if (oldArray[i][j]>threshold) //if the old array is over the threshold
                {
                    newArray[i][j]=1;
                }
                else
                {
                    newArray[i][j]=0;
                }

            }
        }
        return newArray;
    }

    public int[]equcompare(int[]oldArray, boolean threshold)
    {
        int [] newArray = new int [oldArray.length]; //defining new array
        for (int i=0; i < oldArray.length; i++) //
        {

            if (oldArray[i]==1&& threshold==true) //if the old array is the same as the threshold
            {
                newArray[i]=1;
            }
            else
            {
                newArray[i]=0;
            }

        }
        return newArray;
    }
    public double sumarray(double sarray[][]) {
        int sum = 0;
        for(int i=0; i<sarray.length; i++ ) {
            for(int j=0; j<sarray[0].length; j++){
                sum+=sarray[i][j];
            }
        }

        return sum;
    }
    public int sumarray1D(int sarray[]) {
        int sum = 0;
        for(int i=0; i<sarray.length; i++ ) {
            sum+=sarray[i];

        }

        return sum;

    }






    public int [] zeros(int column){
        int anarray[]=new int [column];
        for (int i=0; i<column; i++)
        {
            anarray[i]=0;
        }
        return anarray;
    }






