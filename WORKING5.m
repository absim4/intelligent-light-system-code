clear all, clc, close all;

% Starts webcam
cam = webcam;
preview(cam);
% load('matlab.mat');
%% Declarations
i = 2;
k = 1;
j = 1;
h = 1;
check = 0;
ID_TL = zeros(1,i);
ID_TR = zeros(1,i);
ID_BL = zeros(1,i);
ID_BR = zeros(1,i);


%  figure;
%  hold on;

while j ~= 50
    tic
    % Acquire a single image.
    rgbImage = snapshot(cam);
    
    % Convert RGB to grayscale.
    gray = rgb2gray(rgbImage);
    
%     % Blurring image
%     intImage = integralImage(gray);
%     avgH = integralKernel([1 1 7 7], 1/49);
%     J = integralFilter(intImage, avgH);
%     J = uint8(J);
%     %    figure;
%     %    imshow(J);
    
    % Defining image dimensions
    d = size(gray);
    
    % Segregating quadrants
    TL = gray(1:d(1)/2,1:d(2)/2);
    TR = gray(1:d(1)/2,(d(2)/2)+1:d(2));
    BL = gray((d(1)/2)+1:d(1),1:d(2)/2);
    BR = gray((d(1)/2)+1:d(1),(d(2)/2)+1:d(2));
    
    %% Calculations
    TL(TL < 252) = 0;
    P_TL(i) = sum(TL(:)>252)/numel(TL)*100;
    threshold_TL = sum(P_TL)/sum((P_TL(:)>0));
    
    TR(TR < 252) = 0;
    P_TR(i) = sum(TR(:)>252)/numel(TR)*100;
    threshold_TR = sum(P_TR)/sum((P_TR(:)>0));
    
    BL(BL < 252) = 0;
    P_BL(i) = sum(BL(:)>252)/numel(BL)*100;
    threshold_BL = sum(P_BL)/sum((P_BL(:)>0));
    
    BR(BR < 252) = 0;
    P_BR(i) = sum(BR(:)>252)/numel(BR)*100;
    threshold_BR = sum(P_BR)/sum((P_BR(:)>0));
    
    %% Identifications
    
    % Top Left ID
    ID_TL(i) = logical(P_TL(i) > threshold_TL);
   
    % Top Right ID
    ID_TR(i) = logical(P_TR(i) > threshold_TR);
   
    % Bottom Left ID
    ID_BL(i) = logical(P_BL(i) > threshold_BL);
    
    % Bottom Right ID
    ID_BR(i) = logical(P_BR(i) > threshold_BR);
    
    time(i) = toc;
    elapsedtime = sum(time);
    samprate = i/elapsedtime;
    axis_time(i) = elapsedtime;
%     plot(axis_time,ID_BL);
    subplot(2,1,1),plot(P_BL);
    subplot(2,1,2),plot(check);
    grid on   
  
 if ((P_BL(i) >= P_TL(i)) && (P_BL(i) >= P_TR(i)) && (P_BL(i) >= P_BR(i)))
   check = ID_BL;
end

if ((P_BR(i) >= P_TL(i)) && (P_BR(i) >= P_TR(i)) && (P_BR(i) >= P_BL(i)))
   check = ID_BR;
end

if ((P_TL(i) >= P_BL(i)) && (P_TL(i) >= P_TR(i)) && (P_TL(i) >= P_BR(i)))
   check = ID_TL;
end

if ((P_TR(i) >= P_TL(i)) && (P_TR(i) >= P_BL(i)) && (P_TR(i) >= P_BR(i)))
   check = ID_TR;
end

    if (check(i) == 1 && check(i-1) == 0)
        k = k + 1;
    end  
    
    if (check(i) == 0 && check(i-1) == 1)
        h = h + 1;
    end  
         
   i=i+1;
   j = j+1;
end

%% Confirming identity
     light_ID0 = [1 1 0 0];
     light_ID1 = [1 0 0 0];
     light_ID2 = [1 1 1 1];
     light_ID3 = [1 0 1 0];
zeros = sum(check(:)==0);
ones = sum(check(:)==1);
ratio = round(zeros/ones);
     
      if ((ratio == 1) && (k < 9))
          clc;
        fprintf("You are under light ID '0'\n");
    end
    
     if (ratio >= 2)
         clc;
         fprintf("You are under light ID '1'\n");
    end
    
     if ((ratio == 1) && (k >= 15))  
         clc;
         fprintf("You are under light ID '2'\n");
    end
    
     if ((ratio == 1) && (9 < k) && (k < 14))
         clc;
         fprintf("You are under light ID '3'\n");
     end

    
     

