//255 is off, 0 is on full
//analogWrite supported on pins 3,5,6,9,10,11
//frequence is 490Hz, pins 5,6 has 980Hz

//190-220 flashes (0-255)

int ledPin = 5;
bool up = false;
int max_bright = 100;
int min_bright = 0;
int brightness = max_bright;
int switch_delay = 10;
long test_freq = 500;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void pwm(int duty_cycle,long frequency,int pin){
  //custom pwm function using digitalWrite
  //duty_cycle between 0 and 100
  //frequency maximum 1000
  long time_cycle = 1000.0/frequency;
  long on_time = round(time_cycle*duty_cycle/100);
  long off_time = round(time_cycle-on_time);
  Serial.print("time_cycle: ");
  Serial.println(time_cycle);
  Serial.print("on_time: ");
  Serial.println(on_time);
  Serial.print("off_time: ");
  Serial.println(off_time);
  digitalWrite(pin, HIGH);
  delay(on_time);
  digitalWrite(pin, LOW);
  delay(off_time);
  return;
}

void loop() {
  // put your main code here, to run repeatedly:
  pwm(brightness,test_freq,ledPin);
  delay(switch_delay);
  if(up == true){
    if(brightness < max_bright){
      brightness = brightness + 1;
    }
    else{
      up = false;
      brightness = brightness - 1;
    }
  }
  else{
    if(brightness > min_bright){
      brightness = brightness - 1;
    }
    else{
      up = true;
      brightness = brightness + 1;
    }
  }
}
