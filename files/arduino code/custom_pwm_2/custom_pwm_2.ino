int ledPin = 5;

int code_bits = 4;
int max_id = pow(2, code_bits) - 1;
int min_id = 0;
int current_id = min_id;
bool up = true;

int high = 0;
int low = 255;
int mid = 225;

int id_flash_delay = 500;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void flash_id(int id){
  int current_remainder = id;
  double two_power = 0;
  for (int i = code_bits - 1; i > -1; i = i - 1){
    two_power = pow(2, i);
    if (current_remainder >= two_power){
      current_remainder = current_remainder - two_power;
      analogWrite(ledPin, high);
    }
    else{
      analogWrite(ledPin, low);
    }
    delay(id_flash_delay);
  }
  return;
}

void loop() {
  analogWrite(ledPin, mid);
  delay(1000);
  flash_id(11);
  analogWrite(ledPin, mid);
  delay(1000);
}
