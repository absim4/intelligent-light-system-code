//255 is off, 0 is on full
//analogWrite supported on pins 3,5,6,9,10,11
//frequence is 490Hz, pins 5,6 has 980Hz

int ledPin = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(ledPin, 255);
  delay(1000);
  analogWrite(ledPin, 175);
  delay(1000);
  analogWrite(ledPin, 100);
  delay(1000);
  analogWrite(ledPin, 0);
  delay(1000);
}
