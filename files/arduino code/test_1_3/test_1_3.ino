//255 is off, 0 is on full
//analogWrite supported on pins 3,5,6,9,10,11
//frequence is 490Hz, pins 5,6 has 980Hz

int ledPin = 5;
int brightness = 255;
bool up = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(ledPin, brightness);
  delay(10);
  if(up == true){
    if(brightness < 255){
      brightness = brightness + 1;
    }
    else{
      up = false;
      brightness = brightness - 1;
    }
  }
  else{
    if(brightness > 0){
      brightness = brightness - 1;
    }
    else{
      up = true;
      brightness = brightness + 1;
    }
  }
  
}
