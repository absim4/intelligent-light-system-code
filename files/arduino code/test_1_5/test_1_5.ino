//255 is off, 0 is on full
//analogWrite supported on pins 3,5,6,9,10,11
//frequence is 490Hz, pins 5,6 has 980Hz

//195-225 flashes

int ledPin = 5;
bool up = false;
int max_bright = 255;
int min_bright = 0;
int brightness = max_bright;
int switch_delay = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(ledPin, brightness);
  Serial.println(brightness);
  delay(switch_delay);
  if(up == true){
    if(brightness < max_bright){
      brightness = brightness + 1;
    }
    else{
      up = false;
      brightness = brightness - 1;
    }
  }
  else{
    if(brightness > min_bright){
      brightness = brightness - 1;
    }
    else{
      up = true;
      brightness = brightness + 1;
    }
  }
  
}
